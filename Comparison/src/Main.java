public class Main {
    public static void main(String[] args) {
        int variable1 = 2;
        int variable2 = 4;

        System.out.println("Equality (2 == 4) = " + (variable1 == variable2));
        System.out.println("Not Equal (2 != 4) = " + (variable1 != variable2));
        System.out.println("Less than (2 < 4) = " + (variable1 < variable2));
        System.out.println("Greater than (2 > 4) = " + (variable1 > variable2));
        System.out.println(++variable1); // code is interpreted from left to right
    }
}
