import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        // primitive type int has Integer as the Reference or wrapper class
        ArrayList<Integer> sampleNumbers = new ArrayList<Integer>();

       // List<Integer> sampleNumbers = new ArrayList<>(); // is the same as above but better
        // using List<> instead of explicitly using ArrayList<> enables us to switch to other types
        // and take advantage of the abstract List<> instead
        // List<Integer> sampleNumbers = new ArrayList<>(); type inferred is good
        // sampleNumbers = new LinkedList<>(); valid
        // sampleNumbers = new Stack<>(); valid
        // sampleNumbers = new Vector<>(); valid

        sampleNumbers.add(2);
        sampleNumbers.add(4);
        sampleNumbers.add(6);
        sampleNumbers.add(9);

        System.out.println(sampleNumbers);

        // Enhanced for...loop
        for (int i: sampleNumbers) {
            System.out.println(i);
        }

        // forEach() uses lambda functions
        sampleNumbers.forEach(value -> {
            sampleNumbers.set(sampleNumbers.indexOf(value), value * 2);
        });
        System.out.println("Lambda function results: " + sampleNumbers);

        // ArrayList.size()
        System.out.println("ArrayList size: " + sampleNumbers.size());

        // ArrayList.get(index) Print element based on element index
        System.out.println("Second element: " + sampleNumbers.get(2));

        // ArrayList.indexOf(value) to get the index of given value
        int index = sampleNumbers.indexOf(18);
        System.out.println("Index of 18: " + index);

        // Using Collections to sort(), reverse()
       Collections.sort(sampleNumbers);
       System.out.println("Collections Sort: " + sampleNumbers);

       // Collections.shuffle()
       Collections.shuffle(sampleNumbers);
       System.out.println("Shuffled: " + sampleNumbers);

        // ArrayList.sort(Comparator.naturalOrder()) Sort element
        sampleNumbers.sort(Comparator.naturalOrder());
        System.out.println("Sorted order: " + sampleNumbers);

        // ArrayList.sort(Comparator.reverseOrder()) Reverse order
        sampleNumbers.sort(Comparator.reverseOrder());
        System.out.println("Reverse order: " + sampleNumbers);

        // Update arrayList elements
        // ArrayList.set(index, wrapper.valueOf(element)) #wrapper = Integer, Double, String
        sampleNumbers.set(3,Integer.valueOf(2010));
        System.out.println("Updated: " + sampleNumbers);

        // ArrayList.contains(wrapper.valueOf(element))
        System.out.println("Contains(2010) ? : " + sampleNumbers.contains(Integer.valueOf(2010)));

        // ArrayList.isEmpty()
        System.out.println("isEmpty() ? " + sampleNumbers.isEmpty());

        // ArrayList.remove(index) Remove element from ArrayList based on index
        sampleNumbers.remove(2);
        System.out.println("Remove element at index(2): " + sampleNumbers);

        // ArrayList.remove(wrapper.valueOf(element)) Remove specific element
        sampleNumbers.remove(Integer.valueOf(12));
        System.out.println("Remove element(12): " + sampleNumbers);

        // ArrayList.clear() / Free / Delete entire ArrayList contents
        sampleNumbers.clear();
        System.out.println("Clear ArrayList: " + sampleNumbers);
    }
}
