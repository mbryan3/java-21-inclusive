// This program needs to be updated to use Patterns and Regular expression
public class Main {
    // Mock database entries
    private static final String[] DATABASE = {
            "Product A: Price $100, Category Electronics",
            "Product B: Price $50, Category Books",
            "Product C: Price $150, Category Electronics",
            "Product D: Price $30, Category Apparel",
            // ... more entries
    };

    public static void filterByPriceRange(int min, int max) {
        for (String entry : DATABASE) {
            String[] splitEntry = entry.split(" ");
            for (String part : splitEntry) {
                if (part.startsWith("$")) {
                    int price = Integer.parseInt(part.substring(1));
                    if (price >= min && price <= max) {
                        System.out.println(entry);
                    }
                    break; // Break after finding the first price in the entry
                }
            }
        }
    }


    public static void main(String[] args) {
        filterByPriceRange(50, 150);  // Filters entries with prices between $50 and $150
    }
}
