public class Main {
    public static void main(String[] args) {
        int sampleNumbers[] = {0,1,2,3,4,5,6,7,8,9,10};
        int sum = 0;
        int selectedMultiple = 0;

        // Sum of first 10 integers. Enhanced for...loop only available to arrays
        for (int i : sampleNumbers) {
            sum+= i;
            System.out.println("Sum(" + i + ") = " + sum);
        }
        System.out.println("- - - - - - - - - - - - - ");

        // Multiples of 5
        for (int i = 0; i <= 5 ;i++ ) {
            selectedMultiple = 5 * i;
            System.out.println("Mul(5*" + i + ") = " + selectedMultiple);
        }

        // Multiples of first 10 integers
        for (int i = 0; i <= 10; i++ ) {
            System.out.printf("- - - - - ( %d ) - - - - - \n", i);
            for (int multiplier = 0; multiplier <= 10; multiplier++)
                System.out.printf("Multiplier(%d X %d ) = %d \n", i, multiplier, (i * multiplier));
        }
        System.out.println("- - - - - - - - - - - - - ");

        // while...loop
        int age = 0;
        while (age <= 30) {
            System.out.printf("Age(%d) = Born (%d) \n", age, (2023-age));
            age++;
        }
        System.out.println("- - - - - - - - - - - - - ");

        // do...while loop
        do {
            System.out.printf("Age(%d) = Born (%d) \n", age, (2023-age));
            age++;
        } while (age <= 35);
    }
}
