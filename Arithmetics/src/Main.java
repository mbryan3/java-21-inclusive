public class Main {
    public static void main(String[] args) {
        int variable1 = 2;
        int variable2 = 4;

        // arithmetic operations
        System.out.println("Modulo (4 % 2) = " + (variable2 % variable1));
        System.out.println("Addition (2 + 4) = " + (variable1 + variable2));
        System.out.println("Division (2 / 4) = " + (float) variable1 / variable2);
        System.out.println("Subtraction (2 - 4) = " + (variable1 - variable2));
        System.out.println("Multiplication (2 * 4) = " + (variable1 * variable2));
    }
}
