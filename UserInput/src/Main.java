import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter name: ");
        String name = scanner.nextLine();

        System.out.print("Enter year of birth: ");
        try {
            int age = scanner.nextInt();

            // clean up input buffer of <enter> key after
            scanner.nextLine();

            // OR int age = Integer.parseInt(scanner.nextLine());
            // OR double age = Double.parseDouble(scanner.nextLine());

            System.out.println("Welcome " + name + ". You're " + (2024 - age ) + " years young ");

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        scanner.close();
    }
}
