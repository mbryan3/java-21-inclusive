public class Book {
    private String title;
    private String author;
    private int pageCount;
    private String bookFormat;

    // Constructor
    Book(String title, String author, int pageCount, String bookFormat) {
        this.title = title;
        this.author = author;
        this.pageCount = pageCount;
        this.bookFormat = bookFormat;
    }

    // Custom toString() because we were only receiving memory reference to data
    public String toString() {
        return this.title + " by " + this.author;
    }

    // getters
    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return author;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String getBookFormat() {
        return bookFormat;
    }
}
