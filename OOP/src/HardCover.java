public class HardCover extends Book {

    HardCover(String title, String author, int pageCount) {
        super(title, author, pageCount, "Hard Cover");
    }
}
