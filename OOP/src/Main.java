import java.util.ArrayList;

public class Main {
    public  static void main(String[] any) {
        ArrayList<AudioBook> userAudioBooks = new ArrayList<>();
        ArrayList<Ebook> userEbooks = new ArrayList<>();
        ArrayList<HardCover> userHardCoverBooks = new ArrayList<>();

        User firstUser = new User("Moses", "1993-04-10");

        HardCover sampleBook = new HardCover("1984", "George Orwell", 530);
        HardCover anotherBook = new HardCover("Bartleby, the Scrivener", "Forgotten", 390);

        AudioBook sampleAudioBook = new AudioBook("The Power of Positive thinking", "Norman Vincent Peale", 180);
        AudioBook anotherAudioBook = new AudioBook("Caravan Dragons", "Little Arthur", 120);

        Ebook sampleEbook = new Ebook("Privacy and Power", "Camilla", 164);
        Ebook anotherEbook = new Ebook("Winning in 2024", "Myself", 60);

        System.out.printf(" %s was born on %s and is now %d years young!\n", firstUser.getName(), firstUser.getBirthday(), firstUser.age());

        firstUser.borrow(sampleBook);
        userHardCoverBooks.add(sampleBook);

        firstUser.borrow(anotherBook);
        userHardCoverBooks.add(anotherBook);

        firstUser.borrow(sampleAudioBook);
        userAudioBooks.add(sampleAudioBook);

        firstUser.borrow(anotherAudioBook);
        userAudioBooks.add(anotherAudioBook);

        firstUser.borrow(sampleEbook);
        userEbooks.add(sampleEbook);

        firstUser.borrow(anotherEbook);
        userEbooks.add(anotherEbook);

        System.out.println(firstUser.getName() + " book borrow list: ");
        firstUser.borrowedBooks();

        System.out.println("\n Audio Book List");
        userAudioBooks.forEach(book -> {
            System.out.println(userAudioBooks.indexOf(book) + 1 + " " + book + " runs for " + book.getRunTime() + " minutes");
        });

        System.out.println("\n Ebook List");
        userEbooks.forEach(book -> {
            System.out.println( userEbooks.indexOf(book) + 1 + " " + book +" is a " + book.getBookFormat());
        });
    }
}
