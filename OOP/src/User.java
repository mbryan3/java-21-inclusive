import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class User {
    private String name;
    private LocalDate birthday;
    private ArrayList<Book> bookCatalog = new ArrayList<Book>();
    StringBuilder maxValue

    // Constructor
    User(String name, String birthday) {
        this.name = name;
        this.birthday = LocalDate.parse(birthday);
    }

    // getName() getter # Abstraction
    public String getName() {
        return name;
    }

    // getBirthday() getter # Abstraction
    public LocalDate getBirthday() {
        return birthday;
    }

    // getBookCatalog() getter # Abstraction
    public ArrayList<Book> getBookCatalog() {
        return bookCatalog;
    }

    // age() calculates the user's current age
    public int age() {
        Period userAge = Period.between(this.birthday, LocalDate.now());
        return userAge.getYears();
    }

    // boor(Book someBook) add books to user's book catalog
    public void borrow(Book someBook) {
        this.bookCatalog.add(someBook);
    }

    public String borrowedBooks() {
        bookCatalog.forEach(book -> System.out.println(
                bookCatalog.indexOf(book) + 1 + " - " + book + " | " + book.getPageCount() + " pages | Format " + book.getBookFormat())

        );
        return "";
    }
}
