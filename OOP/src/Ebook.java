public class Ebook extends Book {

    Ebook(String title, String author, int pageCount) {
        super(title, author, pageCount, "Ebook");
    }
}
