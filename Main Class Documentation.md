# Main Class Documentation

The `Main` class contains the `main` method, serving as the entry point for the application. It demonstrates the usage of the `User`, `Book`, `HardCover`, `AudioBook`, and `Ebook` classes by creating instances of users and various types of books.

## Main Method

### `public static void main(String[] any)`

#### Description:
This method creates instances of users and different types of books (HardCover, AudioBook, Ebook), adds these books to the users' book catalogs, and prints information about the users and their borrowed books.

## Example Usage

```java
// Creating instances of users and books
User firstUser = new User("Moses", "1993-04-10");

HardCover sampleBook = new HardCover("1984", "George Orwell", 530);
AudioBook sampleAudioBook = new AudioBook("The Power of Positive Thinking", "Norman Vincent Peale", 180);
Ebook sampleEbook = new Ebook("Privacy and Power", "Camilla", 164);

// Borrowing books and adding to user catalogs
firstUser.borrow(sampleBook);
firstUser.borrow(sampleAudioBook);
firstUser.borrow(sampleEbook);

// Printing user information
System.out.printf("%s was born on %s and is now %d years young!\n", firstUser.getName(), firstUser.getBirthday(), firstUser.age());

// Printing user's borrowed books
System.out.println(firstUser.getName() + " book borrow list: ");
firstUser.borrowedBooks();

// Additional book collections
ArrayList<AudioBook> userAudioBooks = new ArrayList<>();
ArrayList<Ebook> userEbooks = new ArrayList<>();
ArrayList<HardCover> userHardCoverBooks = new ArrayList<>();

// Adding books to specific collections
userHardCoverBooks.add(sampleBook);
userAudioBooks.add(sampleAudioBook);
userEbooks.add(sampleEbook);

// Printing additional book collections
System.out.println("\nAudio Book List");
userAudioBooks.forEach(book -> {
    System.out.println(userAudioBooks.indexOf(book) + 1 + " " + book + " runs for " + book.getRunTime() + " minutes");
});

System.out.println("\nEbook List");
userEbooks.forEach(book -> {
    System.out.println(userEbooks.indexOf(book) + 1 + " " + book + " is a " + book.getBookFormat());
});
```

In this example, users are created and borrow books of different types. The borrowed books are then printed along with additional book collections for AudioBooks and Ebooks. Adjust the method usage and attribute values according to your specific needs.