import java.util.Arrays;

public class Main {
    public static void main() {
        char vowels[] = new char[5];
        String names[] = {"Moses", "Bryan"};

        vowels[0] = 'a';
        vowels[1] = 'o';
        vowels[2] = 'i';
        vowels[3] = 'e';
        vowels[4] = 'u';

        // in-built Arrays has multiple methods such as .toString(array) .binarySearch(array, value)
        // .fill(array, value) .sort(array) .fill(array, startIndex, endIndex, value) .sort(array, startIndex, endIndex, value)
        System.out.println(Arrays.toString(vowels));

        // enhanced for loop
        for (char i : vowels) {
            System.out.println(i);
        }

        char vowels2[] = {'e', 'u', 'a', 'o', 'i'};

        Arrays.sort(vowels2); // in-line sort
        System.out.println(Arrays.toString(vowels2)); // in-line print

        // returns index of key / element being searched if found. Array must be sorted first
        System.out.println("indes of i is " + Arrays.binarySearch(vowels, 'i') + " using binary search");

        Arrays.sort(names);
        System.out.println(Arrays.toString(names));

        // enhanced for loop
        for (String studentNames: names ) {
            System.out.println(studentNames);
        }

        Arrays.fill(vowels, 'x');
        System.out.println(vowels);

        // fill from custom element position
        Arrays.fill(vowels, 1, 3, 'z');
        System.out.println(vowels);

        // copies only reference / address of the original array
        String copyNames[] = names;
        names[0] = "John";

        System.out.println(Arrays.toString(names));

        // Right way to copy
        String copyOfnames[] = Arrays.copyOf(names, names.length);
    }
}
