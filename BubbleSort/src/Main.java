// javac --enable-preview --release 21 Main.java
// java --enable-preview Main

void main() {
    int[] sampleNumbers = {30, 10, 50, 20, 60, 40};

    for(int i = 0; i < sampleNumbers.length; i++) {
        for(int j = 0; j < sampleNumbers.length - i -1; j++) {
            if(sampleNumbers[j] > sampleNumbers[j+1]) {
                int temp = sampleNumbers[j];
                sampleNumbers[j] = sampleNumbers[j+1];
                sampleNumbers[j+1] = temp;
            }
        }
    }

    for(int y: sampleNumbers) {
        System.out.println(y);
    }
}
