public class Main {
    public static void main(String[] args) {
        int stoppingPoint = 10;
        int startPoint = 0, nextValue = 1;
        int count = 1;

        while (count < stoppingPoint - 2) {
            int sum = startPoint + nextValue;
            System.out.print(sum + ", ");

            startPoint = nextValue;
            nextValue = sum;
            count++;
        }
    }
}
