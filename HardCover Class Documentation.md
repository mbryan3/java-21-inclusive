# HardCover Class Documentation

The `HardCover` class extends the `Book` class, specializing in representing hardcover books. It inherits the attributes and methods from the `Book` class and provides a constructor specific to hardcover books.

## Constructors

### `HardCover(String title, String author, int pageCount)`

#### Parameters:
- `title` (String): The title of the hardcover book.
- `author` (String): The author of the hardcover book.
- `pageCount` (int): The total number of pages in the hardcover book.

#### Description:
This constructor creates a new instance of the `HardCover` class by invoking the constructor of its superclass `Book`. It sets the book format to "Hard Cover" by default.

## Example Usage

```java
// Creating a new HardCover instance
HardCover myHardCoverBook = new HardCover("The Catcher in the Rye", "J.D. Salinger", 224);

// Retrieving book information using getters inherited from the Book class
String title = myHardCoverBook.getTitle();
String author = myHardCoverBook.getAuthor();
int pageCount = myHardCoverBook.getPageCount();
String bookFormat = myHardCoverBook.getBookFormat();

// Printing book information using the custom toString() method inherited from the Book class
System.out.println(myHardCoverBook.toString());
```

In this example, a `HardCover` object is created, and its attributes are accessed using the inherited getters from the `Book` class. The custom `toString()` method from the `Book` class is also available for use. Adjust the attribute values and method usage according to your specific needs.