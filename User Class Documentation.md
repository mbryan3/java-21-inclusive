# User Class Documentation

The `User` class represents a user in a library system, including their personal information and a catalog of borrowed books.

## Constructors

### `User(String name, String birthday)`

#### Parameters:
- `name` (String): The name of the user.
- `birthday` (String): The user's date of birth in the format "yyyy-MM-dd."

#### Description:
This constructor creates a new instance of the `User` class and initializes the user's name and birthday.

## Methods

### `public String getName()`

#### Returns:
- The name of the user.

#### Description:
This method allows the retrieval of the user's name.

### `public LocalDate getBirthday()`

#### Returns:
- The user's date of birth as a `LocalDate` object.

#### Description:
This method allows the retrieval of the user's date of birth.

### `public ArrayList<Book> getBookCatalog()`

#### Returns:
- An `ArrayList` containing the user's book catalog.

#### Description:
This method allows the retrieval of the user's book catalog.

### `public int age()`

#### Returns:
- The user's current age.

#### Description:
This method calculates and returns the user's current age based on their date of birth.

### `public void borrow(Book someBook)`

#### Parameters:
- `someBook` (Book): The book to be added to the user's book catalog.

#### Description:
This method adds a book to the user's book catalog.

### `public String borrowedBooks()`

#### Returns:
- A string representation of the user's borrowed books.

#### Description:
This method prints the details of the books in the user's book catalog, including their index, title, author, page count, and format.

## Example Usage

```java
// Creating a new User instance
User user = new User("John Doe", "1990-05-15");

// Borrowing books
Book book1 = new Book("The Alchemist", "Paulo Coelho", 208, "Paperback");
Book book2 = new Ebook("Dune", "Frank Herbert", 694); // Ebook is a subclass of Book
Book book3 = new HardCover("To Kill a Mockingbird", "Harper Lee", 336); // HardCover is a subclass of Book

user.borrow(book1);
user.borrow(book2);
user.borrow(book3);

// Retrieving user information
String userName = user.getName();
LocalDate userBirthday = user.getBirthday();
int userAge = user.age();
ArrayList<Book> userBookCatalog = user.getBookCatalog();

// Printing borrowed books
user.borrowedBooks();
```

In this example, a `User` object is created, books are borrowed and added to the user's book catalog, and user information is retrieved and printed. Adjust the method usage and attribute values according to your specific needs.