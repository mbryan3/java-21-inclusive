public class Main {
  private byte age =  29;
  private byte houseNumber = 8;
  private int maxInt = Integer.MAX_VALUE;
  String name = "Moses";
  String sayName = new String("Moses");
  int[] dogAges = {19, 20, 21};
  String[] treeNames = {"John", "Peter", "Mary"};

    public static void main(String[] args) {
        sayLess();
    }

    private static byte doMath(byte a, byte b) {
       return (byte) (a+b);
    }

    private static void sayLess() {
        Main instanceOfMain = new Main();

        System.out.println(instanceOfMain.maxInt + " <- MAX_VALUE int\n" +  doMath(instanceOfMain.age, instanceOfMain.houseNumber));
        System.out.println(instanceOfMain.name == instanceOfMain.sayName); // false -> this is object vs primitive
        System.out.println(instanceOfMain.name.equals(instanceOfMain.sayName)); // true // evaluates content

        for (int i: instanceOfMain.dogAges ) {
            System.out.println(i);
        }

        for (String i: instanceOfMain.treeNames) {
            System.out.println(i);
        }
    }
}
