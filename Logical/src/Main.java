public class Main {
    public static void main(String[] args) {
        int variable1 = 10;
        boolean even = true;

        // AND, OR
        if (even && variable1 % 2 == 0) {
            System.out.println("Number is indeed even");
        }

    }
}
