import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to Calculator!");

        System.out.print("Enter first number: ");
        double variable1 = Double.parseDouble(scanner.nextLine());

        System.out.print("Enter second number: ");
        double variable2 = Double.parseDouble(scanner.nextLine());

        System.out.print("What operations would you like to perform? + / - * % ");
        String operation = scanner.nextLine();

        performOperation(variable1, variable2, operation);

        scanner.close();
    }

    public static void performOperation(double variable1, double variable2, String operation) {
        switch (operation) {
            case "+" -> System.out.println(variable1 + variable2);
            case "/" -> System.out.println(variable1 / variable2);
            case "-" -> System.out.println(variable1 - variable2);
            case "*" -> System.out.println(variable1 * variable2);
            case "%" -> System.out.println(variable1 % variable2);
            default -> System.out.println("Wrong operation! Try again");
        }
    }
}
