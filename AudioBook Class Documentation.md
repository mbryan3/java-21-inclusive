# AudioBook Class Documentation

The `AudioBook` class extends the `Book` class and introduces a specialization for audio books. It includes an additional attribute, `runTime`, representing the total duration of the audio book in minutes.

## Constructors

### `AudioBook(String title, String author, int runTime)`

#### Parameters:
- `title` (String): The title of the audio book.
- `author` (String): The author of the audio book.
- `runTime` (int): The total duration of the audio book in minutes.

#### Description:
This constructor creates a new instance of the `AudioBook` class by invoking the constructor of its superclass `Book`. It sets the page count to 0 (since audio books don't have pages) and the book format to "audio." The `runTime` attribute is initialized with the provided value.

## Methods

### `public int getRunTime()`

#### Returns:
- The total duration of the audio book in minutes.

#### Description:
This method allows the retrieval of the `runTime` attribute, providing information about the duration of the audio book.

## Example Usage

```java
// Creating a new AudioBook instance
AudioBook myAudioBook = new AudioBook("The Hitchhiker's Guide to the Galaxy", "Douglas Adams", 300);

// Retrieving book information using getters inherited from the Book class
String title = myAudioBook.getTitle();
String author = myAudioBook.getAuthor();
int pageCount = myAudioBook.getPageCount(); // Returns 0 for audio books
String bookFormat = myAudioBook.getBookFormat();

// Retrieving AudioBook-specific information using the getRunTime() method
int runTime = myAudioBook.getRunTime();

// Printing book information using the custom toString() method inherited from the Book class
System.out.println(myAudioBook.toString() + " - " + runTime + " minutes");
```

In this example, an `AudioBook` object is created, and both the inherited methods from the `Book` class and the specific method from the `AudioBook` class are used to retrieve information. Adjust the attribute values and method usage according to your specific needs.