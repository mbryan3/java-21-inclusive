# Ebook Class Documentation

The `Ebook` class extends the `Book` class, specializing in representing electronic books (e-books). It inherits the attributes and methods from the `Book` class and provides a constructor specific to e-books.

## Constructors

### `Ebook(String title, String author, int pageCount)`

#### Parameters:
- `title` (String): The title of the e-book.
- `author` (String): The author of the e-book.
- `pageCount` (int): The total number of pages in the e-book.

#### Description:
This constructor creates a new instance of the `Ebook` class by invoking the constructor of its superclass `Book`. It sets the book format to "Ebook" by default.

## Example Usage

```java
// Creating a new Ebook instance
Ebook myEbook = new Ebook("1984", "George Orwell", 328);

// Retrieving book information using getters inherited from the Book class
String title = myEbook.getTitle();
String author = myEbook.getAuthor();
int pageCount = myEbook.getPageCount();
String bookFormat = myEbook.getBookFormat();

// Printing book information using the custom toString() method inherited from the Book class
System.out.println(myEbook.toString());
```

In this example, an `Ebook` object is created, and its attributes are accessed using the inherited getters from the `Book` class. The custom `toString()` method from the `Book` class is also available for use. Adjust the attribute values and method usage according to your specific needs.