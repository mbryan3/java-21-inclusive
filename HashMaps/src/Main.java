import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashMap<String, Integer> scores = new HashMap<String, Integer>();
        scores.put("Algorithms", 95);
        scores.put("Discrete Structures", 90);
        scores.put("Software Development", 92);
        scores.put("Big Data Analytics", 90);

        // putIfAbsent() updates HashMap if key doesn't exit
        scores.putIfAbsent("Maths", 99);

        // getOrDefault(data, integerValue) the integerValue can be treated as error code
        System.out.println(scores.getOrDefault("Searching for: OOP in Java: ", 0));

        // loop through HashMap
        scores.forEach((key, value) -> {
            System.out.println("key: " + key + " value: " + value);

            // improved switch()
            switch (key) {
                case "Algorithms" -> System.out.println("Algorithms and Data Structures");
                case "Maths" -> System.out.println("Maths and Statics");
                default -> System.out.println(key);
            }

            System.out.println("- - - - - - - - - - - - - - - - -");
        });
        System.out.println("HashMap.size(): " + scores.size());

        System.out.println(scores.toString());

        // check if containsKey(StringData) or containsValue(IntegerData)
        System.out.println("Contains \"Big Data Analytics\" : " + scores.containsKey("Big Data Analytics"));
        System.out.println("Contains value 92: " + scores.containsValue(92));
        System.out.println("Contains value 90: " + scores.containsValue(Integer.valueOf(90)));

        // .replace(key, oldValue, newValue) to update contents
        // .replace(key, newValue)
        scores.replace("Discrete Structures", 90, 93);

        // .remove(data) to remove particular key
        scores.remove("Maths");
        System.out.println("Removed Maths: " + scores);

        // .clear() to erase contents of the HashMap
        scores.clear();

        // .isEmpty()
        System.out.println("isEmpty(scores): " + scores.isEmpty());
    }
}
