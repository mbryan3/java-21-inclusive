
void main() {
    String name = "Moses Bryan";
    System.out.printf("Hello " + name + ", welcome!\n");

    sayName();
}

String sayName() {
    System.out.println("Feel free to say something?");
    return "";
}

// javac --enable-preview --release 21 -Xlint:preview Main.java
// java --enable-preview Main
