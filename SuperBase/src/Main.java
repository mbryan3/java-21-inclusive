// You don't actually need this, but it's good practice to keep it to avoid confusion
// The compiler is looking for main()
//public class Main {
//
//}

class Base {
    void setup() {
        System.out.println("Base setup");
    }

    Base() {
        System.out.println("Default Base Constructor");
        setup();
    }
}

class DerivedBase extends Base {
    private int someValue;

    @Override
    void setup() {
        someValue = 21;
        System.out.println("Derived setup Constructor value = " + someValue);
    }

    DerivedBase() {
        System.out.println("Derived Default Constructor");
    }

    public static void main(String[] any) {
        DerivedBase derivative = new DerivedBase();
        System.out.println(derivative.someValue); // unexpected value behavior 21 or 0 ?
    }
}

// Base constructor is executed first, followed by setup which is overridden then
// the default constructor for DerivedBase gets called last
// Notice that main(String[] any) is in the DerivedBase class
