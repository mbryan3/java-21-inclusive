public class Main {
    public static void main(String[] args) {
        Person randomPerson = new Person("Katt", 21);
        randomPerson.speak();
        randomPerson.personCount();

        Person anotherPerson = new Person("Kevin", 19);
        anotherPerson.speak();
        anotherPerson.personCount();

        Person thirdPerson = new Person("Steve", 20);
        thirdPerson.speak();
        thirdPerson.personCount();
    }
}
class Person {
    private String name;
    private int age;
    private static int personCount = 0;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
        this.personCount++;
    }

    void speak() {
        System.out.println("I\'m " + name + " and I\'m " + age + " years young.");
    }

    void personCount() {
        System.out.println(personCount + " person(s) entered the chat \n");
    }
}
