public class Main {
    public static void main(String[] args) {
        // literal strings vs object strings
        // .lenght() .isEmpty() .toUpperCase() .toLowerCase() .equals() .equalsIgnoreCase()
        // .replace("abc", "xyz") .contains("efg")

        String literalString1 = "abc";
        String literalString2 = "abc";

        String objectString1 = new String("xyz");
        String objectString2 = new String("xyz");

        System.out.println(literalString1 == literalString2); // true
        System.out.println(objectString1 == objectString2); // false
        System.out.println(objectString1.equals(objectString2)); // true
    }
}
