# Book Class Documentation

## Class Overview

The `Book` class represents a book with essential attributes such as title, author, page count, and book format. It provides a constructor for initializing these attributes and getter methods for accessing the information.

## Constructors

### `Book(String title, String author, int pageCount, String bookFormat)`

#### Parameters:
- `title` (String): The title of the book.
- `author` (String): The author of the book.
- `pageCount` (int): The total number of pages in the book.
- `bookFormat` (String): The format of the book (e.g., hardcover, paperback, eBook).

#### Description:
This constructor creates a new instance of the `Book` class and initializes its attributes with the provided values.

## Methods

### `public String toString()`

#### Returns:
- A formatted string representing the book's title and author.

#### Description:
This method overrides the default `toString()` method to provide a custom string representation of the `Book` object. It returns a string in the format: "title by author."

### Getters

#### `public String getAuthor()`

#### Returns:
- The author of the book.

#### `public String getTitle()`

#### Returns:
- The title of the book.

#### `public int getPageCount()`

#### Returns:
- The total number of pages in the book.

#### `public String getBookFormat()`

#### Returns:
- The format of the book (e.g., hardcover, paperback, eBook).

## Example Usage

```java
// Creating a new Book instance
Book myBook = new Book("The Great Gatsby", "F. Scott Fitzgerald", 180, "Paperback");

// Retrieving book information using getters
String title = myBook.getTitle();
String author = myBook.getAuthor();
int pageCount = myBook.getPageCount();
String bookFormat = myBook.getBookFormat();

// Printing book information using the custom toString() method
System.out.println(myBook.toString());
```

In this example, a `Book` object is created with specific attributes, and information is retrieved using the provided getters and the custom `toString()` method. Adjust the attribute values and method usage according to your specific needs.