public class Main {
    public static void main(String[] args) {
        int[] sampleNumbers = {1, 2, 3, 4, 5};
        int target = 3;
        int targetIndex;

        // enhanced for loop doesn't work here because it iterates over actual values instead of indices
        for (int i = 0; i < sampleNumbers.length; i++) {
            if (sampleNumbers[i] == target) {
                targetIndex = i;
                System.out.printf("Found %d at index %d \n", target, targetIndex);
            }
        }
    }
}
